package com.chenxin.springcloud.service;

/**
 * @Author ChenXin
 * @Since 2020/3/14 14:13
 */
public interface IMessageProvider {

    public String send();
}
