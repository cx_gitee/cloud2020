package com.chenxin.springcloud.alibaba.service;

import com.chenxin.springcloud.alibaba.domain.Order;
import com.chenxin.springcloud.alibaba.domain.Order;

/**
 * @Author ChenXin
 * @Since 2020/3/18 21:17
 */
public interface OrderService {

    /**
     * 创建订单
     * @param order
     */
    void create(Order order);
}
