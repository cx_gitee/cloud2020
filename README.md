关于SpringCloud2020的学习项目

## 项目简介:

该项目是跟着视频自己搭建出来的，里面涉及传统的 eureka、hystrix、ribbon，更是讲解了最新的 alibaba的 Nacos和sentinel、Seata，相当的给力。

该项目中有我按照视频的内容总结的思维导图，同样是mmap格式的。

与视频不同的是，我在思维导图中加入了一些我配置组件遇到的部分问题的解决方案，如果有和我一样，可以参考思维导图。

另外是推荐小伙伴们还是得多看官网的文档，视频只是引路人，很多东西是需要通过自己总结说出来才算正在掌握了的。



## 官网文档传送门：

SpringCloud: https://spring.io/projects/spring-cloud/

这个网址是各springcloud组件的配置介绍，自己搭建组件环境可以考虑看这个。

Seata： https://seata.io/zh-cn/docs/overview/what-is-seata.html

分布式事务解决的框架，文档介绍很详细，推荐。

Nacos： https://nacos.io/zh-cn/docs/what-is-nacos.html

那个替代Eureka和Config的男人。

Sentinel：[https://github.com/alibaba/Sentinel/wiki/%E4%BB%8B%E7%BB%8D](https://github.com/alibaba/Sentinel/wiki/介绍)

在Hystrix基础上增加了流控规则和持久化,alibaba体系的一员。









