package com.chenxin.springcloud.alibaba.service;


/**
 * @Author ChenXin
 * @Since 2020/3/18 22:58
 */
public interface StorageService {

    void decrease(Long productId, Integer count);

}
